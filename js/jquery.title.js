/* Customisation du titre pour le theme Infini (uniquement si le nom du site est exatement "Association Infini") */

$(document).ready(function(){
    
    window_width = $(window).width();
    title = $('#logo_site_spip');
    html_title = $('#logo_site_spip').html();
    html_title = html_title.replace('Association Infini','Association <span>In</span>ternet <span>Fini</span>stère')
    
    if (window_width > 450){ //Seulement 
        title.html(html_title);
        //    $('#spip-admin').append('title: '+html_title);
    }

});