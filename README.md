# Thème SPIPr pour le site infini.fr

Ce thème utilise les mots-clés de configurations suivants :
(voir la description des mot-clés dans le spip)

groupe conf_articles
- inclu-menu
- presentation
- masque

groupe conf_rubriques
- actus
- services
- exclu-menu
- large
- liste-sans-intro

Ainsi que le mot-clé éditorial "edito".

## Historique des versions

1.5.0: 
- Nouveau topmenu

1.3.0: [partiel]
- Réorganisation complète des feuilles de style pour correspondre a la doc spipr

1.4.0
- Modifications pour intégration du nouveau topmenu (harmoniser avec les futurs framaservices)
- Ajout de la div container #site

1.3.4
- Correction de l'expose des articles en sous-menus (inclu-menu)

1.3.3
- Modifications des listes rubriques/articles
- Correction icones dans documents joints
- Ajout icones top-menu
- Date de publication dans le aside uniquement pour les articles syndiqués
- Ajout du mot-clé "large"

1.3.2
- Ajout du chapeau dans l'edito

1.3.1
- Style pour la page etats des services
- On désactives les lettrines

1.3.0
- Finalement on abandonne boot-theme.less et on regroupe tout dans theme.less parce que bas sinon c'est trop le bordel (-_-)'

1.2.4:
- Gestion de la classe .no-drop-cap (qui permet de ne pas afficher de lettrine) en amont plutot qu'en javascript pour les resumes d'articles (plus de conflit avec la pagination ajax)
- Ajout du fil d'arianne
- Restylisation header .chapo, .texte, liens, strong
- Repositionement du footer

1.2.3:
- Modification de la taille des titres
- Modification des lettrines
- Modification de .articles .introduction
- Modification apparences hover focus menus

1.2.2:
- Ajout de la balise noscript
- Ajout des balises meta og 

1.2.1:
- Restylisation titres et strong

1.2.1:
- Top-menu et boutons

1.2.0:
- Resonsive \o/ (layouts.less)

1.1.0:
- Première réorganisation des feuilles de style pour correspondre a la doc spipr (ajout boot-theme.less)

1.0.7:
- Limitation à trois mots-clés pour les résumés d'articles dans les listes

1.0.6:
- Mise en page d'acceuil de l'article avec le mot clé presentation
- Ajustement de la liste des tags et publication
- Ajustement de la taille des images des boutons de buttons-nav-welcome
- Texte sur une seule ligne pour les menus en liste de aside 

1.0.5:
- Correction de .postmeta et .publication qui peuvent dépassés dans les listes 3 colonnes

1.0.4:
- Correction sur le mot clé edito (son masquage css créait un décalage)

1.0.3:
- Limitation de la justification du texte à certains paragraphes (.texte, .chapo, #description_site_spip)
- Rajout de la prise en charge du menu etats des services
- Ajustement sur les listes et titres dans aside (.extra et .footer)
- Rajout d'un trait sous les h1
- On abandonne les colonnes (.texte, .chapo) ...pour l'instant
- Activation de menu_services
- Ajout du message si il n'y a aucun commentaire à l'article

1.0.2:
- Légère modification des sous menus de la navigation principal(rubriques)
- Correction du texte trop petit pour les chapeaux
- Correction de no-drop-cap.js qui ne s'appliquait pas dans les sections

1.0.1:
- Correction des titres trop long du menu de navigation principal(rubriques) qui peuvent dépasser